var clock =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var clock_1 = __webpack_require__(1);
var datepicker_1 = __webpack_require__(2);
datepicker_1.datepicker();
var current = new clock_1.Clock().createClock();
var usa = new clock_1.Clock('Asia/Tokyo').createClock();
var australia = new clock_1.Clock('Australia/Sydney').createClock();
var europe = new clock_1.Clock('Europe/London').createClock();


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var Clock = (function () {
    function Clock() {
        var timeZone = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            timeZone[_i] = arguments[_i];
        }
        this.width = 200;
        this.height = this.width;
        this.offSetX = 100;
        this.offSetY = 100;
        this.radius = (this.width / 2 - 20);
        this.fontSize = 12;
        this.timeZone = null;
        if (timeZone.length > 0) {
            this.timeZone = timeZone[0];
        }
    }
    Clock.prototype.createClock = function () {
        this.newContainerClock();
        this.renderCircle();
        this.renderTextLbl();
        this.createDescription();
        this.renderTime();
    };
    Clock.prototype.getTimeObj = function (date) {
        console.log(date);
        var second = date.second();
        var minute = date.minute() + second / 60;
        var hour = date.hour() + minute / 60;
        var res = [
            {
                "unit": "seconds",
                "numeric": second
            }, {
                "unit": "minutes",
                "numeric": minute
            }, {
                "unit": "hours",
                "numeric": hour
            }
        ];
        return res;
    };
    Clock.prototype.getTime = function () {
        var time = this.timeZone ? moment().tz(this.timeZone) : moment();
        return time;
    };
    Clock.prototype.newContainerClock = function () {
        var min = 1, max = 100;
        this.idClock = Math.floor(Math.random() * (max - min + 1)) + min;
        var singleClock = document.createElement('div');
        singleClock.id = 'clock-' + this.idClock;
        $('.clocks-container').append(singleClock);
    };
    Clock.prototype.createDescription = function () {
        var current = this.getTime().format('MMMM Do YYYY, h:mm a');
        var place = this.timeZone ? this.timeZone : 'Current';
        var status = this.getTime().endOf('day').fromNow();
        var template = Handlebars.compile("<div class=\"description-info\">\n                <h4> {{place}} </h4>\n                <div class=\"body\">\n                  {{current}}\n                </div>\n                <div class=\"status\"> {{ status }}</div>\n            </div>\n        ");
        var context = {
            place: place,
            current: current,
            status: status
        };
        $('#clock-' + this.idClock).append(template(context));
    };
    Clock.prototype.renderCircle = function () {
        this.visual = d3.selectAll('#clock-' + this.idClock)
            .append("svg:svg")
            .attr("width", this.width)
            .attr("height", this.height);
        this.clockGroup = this.visual.append("svg:g")
            .attr("transform", "translate(" + this.offSetX + "," + this.offSetY + ")");
        this.clockGroup.append("svg:circle")
            .attr("r", this.radius).attr("fill", "#fffbd8")
            .attr("class", "clock outercircle")
            .attr("stroke", "orange")
            .attr("stroke-width", 2);
        this.clockGroup.append("svg:circle")
            .attr("r", this.radius + 7).attr("fill", "none")
            .attr("class", "clock outercircle")
            .attr("stroke", "#1c94c4")
            .attr("stroke-width", 5);
        this.clockGroup.append("svg:circle")
            .attr("r", 4)
            .attr("fill", "orange")
            .attr("class", "clock innercircle");
    };
    Clock.prototype.renderTextLbl = function () {
        var radius = this.radius;
        var fontSize = this.fontSize;
        this.tickLabelGroup = this.visual.append("svg:g").attr("transform", "translate(" + this.offSetX + "," + this.offSetY + ")");
        this.tickLabelGroup.selectAll("text.label")
            .data(d3.range(12))
            .enter().append("svg:text")
            .attr("class", "label")
            .attr("font-size", this.fontSize)
            .attr("x", function (d, i) { return ((radius - fontSize)) * Math.cos(2 * i * 0.26 - 1.57); })
            .attr("y", function (d, i) { return 7 + ((radius - fontSize)) * Math.sin(2 * i * 0.26 - 1.57); })
            .attr("fill", '#000')
            .attr("text-anchor", "middle")
            .text(function (d, i) {
            if (d == 0)
                return 12;
            else {
                return d;
            }
            ;
        });
    };
    Clock.prototype.renderArrows = function (data) {
        var hourArc, minuteArc, secondArc;
        var pi = Math.PI;
        var scaleSecs = d3.scaleLinear().domain([0, 59 + 999 / 1000]).range([0, 2 * pi]);
        var scaleMins = d3.scaleLinear().domain([0, 59 + 59 / 60]).range([0, 2 * pi]);
        var scaleHours = d3.scaleLinear().domain([0, 11 + 59 / 60]).range([0, 2 * pi]);
        this.clockGroup.selectAll('.clockhand').remove();
        secondArc = d3.arc()
            .innerRadius(0)
            .outerRadius(70)
            .startAngle(function (d) {
            return scaleSecs(d.numeric);
        })
            .endAngle(function (d) {
            return scaleSecs(d.numeric);
        });
        minuteArc = d3.arc()
            .innerRadius(0)
            .outerRadius(70)
            .startAngle(function (d) {
            return scaleMins(d.numeric);
        })
            .endAngle(function (d) {
            return scaleMins(d.numeric);
        });
        hourArc = d3.arc()
            .innerRadius(0)
            .outerRadius(50)
            .startAngle(function (d) {
            return scaleHours(d.numeric % 12);
        })
            .endAngle(function (d) {
            return scaleHours(d.numeric % 12);
        });
        this.clockGroup.selectAll('.clockhand')
            .data(data)
            .enter()
            .append('svg:path')
            .attr("d", function (d) {
            if (d.unit === "seconds") {
                return secondArc(d);
            }
            else if (d.unit === "minutes") {
                return minuteArc(d);
            }
            else if (d.unit === "hours") {
                return hourArc(d);
            }
        })
            .attr("class", "clockhand")
            .attr("stroke", "black")
            .attr("stroke-width", function (d) {
            if (d.unit === "seconds") {
                return 1;
            }
            else if (d.unit === "minutes") {
                return 2;
            }
            else if (d.unit === "hours") {
                return 3;
            }
        })
            .attr("fill", "none");
    };
    Clock.prototype.renderTime = function () {
        setInterval(function () {
            var data = this.getTime();
            var time = this.getTimeObj(data);
            return this.renderArrows(time);
        }.bind(this), 1000);
    };
    return Clock;
}());
exports.Clock = Clock;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
function datepicker() {
    var selectDate;
    setInterval(function () {
        var date = moment(new Date()).format('LTS');
        $('#current-value').text(date);
    }, 1000);
    $('#datepicker').datepicker({
        onSelect: function (dateText, inst, extensionRange) {
            selectDate = new Date(dateText);
            var momentDate = moment(dateText).format('ll');
            var leftDate = moment(dateText).fromNow();
            $('#select-value').text(momentDate);
            $('#moment-value').text(leftDate);
        }
    });
}
exports.datepicker = datepicker;
;


/***/ })
/******/ ]);