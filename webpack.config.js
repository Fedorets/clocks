var webpack = require('webpack'),
    path = require('path');

module.exports = {
    entry: './src/ts/main.ts',
    output: {
        filename: './dist/scripts.js',
        library: 'clock'
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        loaders: [
            { test: /\.tsx?$/, loader: "ts-loader" }
        ]
    }
};