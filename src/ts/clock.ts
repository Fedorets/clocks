declare const d3: any;
declare var moment: any;
declare const $: any;
declare const Handlebars: any;

export class Clock {
    private width: number = 200;
    private height: number = this.width;
    private offSetX: number = 100;
    private offSetY: number = 100;
    private idClock: number;
    private visual;
    private clockGroup;
    private tickLabelGroup;
    private radius: number = (this.width/2 - 20);
    private fontSize: number = 12;
    private timeZone: string = null;

    public constructor(...timeZone) {
        if(timeZone.length > 0) {
            this.timeZone = timeZone[0];
        }
    }

    public createClock() {
        this.newContainerClock();
        this.renderCircle();
        this.renderTextLbl();
        this.createDescription();
        this.renderTime();
    }

    public getTimeObj(date) {
        console.log(date)
        let second = date.second();
        let minute = date.minute() + second / 60;
        let hour = date.hour() + minute / 60;
        let res =  [
            {
                "unit": "seconds",
                "numeric": second
            }, {
                "unit": "minutes",
                "numeric": minute
            }, {
                "unit": "hours",
                "numeric": hour
            }
        ];
        return res;
    }

    public getTime() {
        let time = this.timeZone ? moment().tz(this.timeZone) : moment();
        return time;
    }

    public newContainerClock() {
        let min = 1, max = 100;
        this.idClock = Math.floor(Math.random() * (max - min + 1)) + min;
        let singleClock = document.createElement('div');
        singleClock.id = 'clock-'+this.idClock;
        $('.clocks-container').append(singleClock);
    }

    public createDescription() {
        let current = this.getTime().format('MMMM Do YYYY, h:mm a');
        let place = this.timeZone ? this.timeZone : 'Current';
        let status = this.getTime().endOf('day').fromNow();
        let template = Handlebars.compile(
            `<div class="description-info">
                <h4> {{place}} </h4>
                <div class="body">
                  {{current}}
                </div>
                <div class="status"> {{ status }}</div>
            </div>
        `);
        let context = {
            place: place,
            current: current,
            status: status
        };
        $('#clock-'+this.idClock).append(template(context));
    }

    public renderCircle() {
        this.visual = d3.selectAll('#clock-'+this.idClock)
            .append("svg:svg")
            .attr("width", this.width)
            .attr("height", this.height);

        this.clockGroup = this.visual.append("svg:g")
            .attr("transform", "translate(" + this.offSetX + "," + this.offSetY + ")");

        this.clockGroup.append("svg:circle")
            .attr("r", this.radius).attr("fill", "#fffbd8")
            .attr("class", "clock outercircle")
            .attr("stroke", "orange")
            .attr("stroke-width", 2);

        this.clockGroup.append("svg:circle")
            .attr("r", this.radius + 7).attr("fill", "none")
            .attr("class", "clock outercircle")
            .attr("stroke", "#1c94c4")
            .attr("stroke-width", 5);

        this.clockGroup.append("svg:circle")
            .attr("r", 4)
            .attr("fill", "orange")
            .attr("class", "clock innercircle");
    }

    public renderTextLbl() {
        let radius = this.radius;
        let fontSize = this.fontSize;
        this.tickLabelGroup=this.visual.append("svg:g").attr("transform", "translate(" + this.offSetX + "," + this.offSetY + ")");
        this.tickLabelGroup.selectAll("text.label")
            .data(d3.range(12))
            .enter().append("svg:text")
            .attr("class", "label")
            .attr("font-size", this.fontSize)
            .attr("x", function(d, i){return ((radius - fontSize))*Math.cos(2*i*0.26-1.57)  })
            .attr("y", function(d, i){return 7+((radius - fontSize))*Math.sin(2*i*0.26-1.57)   })
            .attr("fill", '#000')
            //    .attr("alignment-baseline", "middle")
            .attr("text-anchor", "middle")
            .text(function(d, i)
                {
                    if (d==0)
                        return 12;
                    else { return d };
                }
            );
    }

    public renderArrows(data) {
        let hourArc, minuteArc, secondArc;
        let pi = Math.PI;
        let scaleSecs = d3.scaleLinear().domain([0, 59 + 999/1000]).range([0, 2 * pi]);
        let scaleMins = d3.scaleLinear().domain([0, 59 + 59/60]).range([0, 2 * pi]);
        let scaleHours = d3.scaleLinear().domain([0, 11 + 59/60]).range([0, 2 * pi]);
        this.clockGroup.selectAll('.clockhand').remove();

        secondArc = d3.arc()
            .innerRadius(0)
            .outerRadius(70)
            .startAngle(function(d) {
                return scaleSecs(d.numeric);
            })
            .endAngle(function(d) {
                return scaleSecs(d.numeric);
            });
        minuteArc = d3.arc()
            .innerRadius(0)
            .outerRadius(70)
            .startAngle(function(d) {
                return scaleMins(d.numeric);
            })
            .endAngle(function(d) {
                return scaleMins(d.numeric);
            });

        hourArc = d3.arc()
            .innerRadius(0)
            .outerRadius(50)
            .startAngle(function(d) {
                return scaleHours(d.numeric % 12);
            })
            .endAngle(function(d) {
                return scaleHours(d.numeric % 12);
            });

        this.clockGroup.selectAll('.clockhand')
            .data(data)
            .enter()
            .append('svg:path')
            .attr("d", function(d) {
                if (d.unit === "seconds") {
                    return secondArc(d);
                } else if (d.unit === "minutes") {
                    return minuteArc(d);
                } else if (d.unit === "hours") {
                    return hourArc(d);
                }
            })
            .attr("class", "clockhand")
            .attr("stroke", "black")
            .attr("stroke-width", function(d) {
                if (d.unit === "seconds") {
                    return 1;
                } else if (d.unit === "minutes") {
                    return 2;
                } else if (d.unit === "hours") {
                    return 3;
                }
            })
            .attr("fill", "none");
    }

    public renderTime() {
        setInterval(function() {
            let data = this.getTime();
            let time = this.getTimeObj(data);
            return this.renderArrows(time);
        }.bind(this), 1000);
    }
}