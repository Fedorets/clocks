import { Clock } from './clock';
import { datepicker } from './datepicker';

datepicker();

let current = new Clock().createClock();
let usa = new Clock('Asia/Tokyo').createClock();
let australia = new Clock('Australia/Sydney').createClock();
let europe = new Clock('Europe/London').createClock();

