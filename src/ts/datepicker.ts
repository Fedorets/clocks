declare const $: any;
declare const Handlebars: any;
declare var moment: any;

export function datepicker() {
    let selectDate;
    setInterval(function () {
        var date = moment(new Date()).format('LTS');
        $('#current-value').text(date);
    }, 1000);
    $('#datepicker').datepicker({
        onSelect: function (dateText, inst, extensionRange) {
            selectDate = new Date(dateText);
            var momentDate = moment(dateText).format('ll');
            var leftDate = moment(dateText).fromNow();
            $('#select-value').text(momentDate);
            $('#moment-value').text(leftDate);
        }
    });
};
